module Assets exposing (Assets, MainObject(..), Msg, TexturedPhysicsObject, duck, init, load, update)

import Color exposing (Color)
import Http
import Length
import Meshes exposing (ConvexMesh, TexturedMesh)
import Obj.Decode
import Physics.Coordinates exposing (BodyCoordinates)
import Scene3d.Material as Material exposing (Texture)
import Task
import WebGL.Texture


type MainObject
    = Duck


duckInfo =
    { objectTextureFile = "Duckling.png"
    , objectMeshFile = "Duckling.obj.txt" -- .txt is required to work with `elm reactor`
    , meshDecoder = Meshes.physics1render1 "convex" "mesh"
    }


type Assets
    = Assets
        { duckMaterial : Maybe (Material.Textured BodyCoordinates)
        , duckConvexMesh : Maybe ConvexMesh
        , duckMesh : Maybe TexturedMesh
        }


init : Assets
init =
    Assets
        { duckMaterial = Nothing
        , duckConvexMesh = Nothing
        , duckMesh = Nothing
        }


load : Cmd Msg
load =
    Cmd.batch
        [ Material.load duckInfo.objectTextureFile
            |> Task.attempt (LoadedTexture Duck)
        , Http.get
            { url = duckInfo.objectMeshFile
            , expect = Obj.Decode.expectObj (LoadedMeshes Duck) Length.meters duckInfo.meshDecoder
            }
        ]


type Msg
    = LoadedTexture MainObject (Result WebGL.Texture.Error (Texture Color))
    | LoadedMeshes MainObject (Result Http.Error ( ConvexMesh, TexturedMesh ))


update : Msg -> Assets -> Assets
update msg (Assets assets) =
    case msg of
        LoadedTexture Duck result ->
            Assets
                { assets
                    | duckMaterial =
                        result
                            |> Result.map Material.texturedMatte
                            |> Result.toMaybe
                }

        LoadedMeshes Duck result ->
            case result of
                Ok ( convex, mesh ) ->
                    Assets
                        { assets
                            | duckConvexMesh = Just convex
                            , duckMesh = Just mesh
                        }

                Err _ ->
                    Assets assets


type alias TexturedPhysicsObject =
    { material : Material.Textured BodyCoordinates
    , convex : ConvexMesh
    , mesh : TexturedMesh
    }


duck : Assets -> Maybe TexturedPhysicsObject
duck (Assets assets) =
    Maybe.map3
        (\a b c -> { material = a, convex = b, mesh = c })
        assets.duckMaterial
        assets.duckConvexMesh
        assets.duckMesh
