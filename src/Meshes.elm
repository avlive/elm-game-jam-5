module Meshes exposing (ConvexMesh, TexturedMesh, physics1render1)

import Frame3d exposing (Frame3d)
import Length exposing (Meters)
import Obj.Decode exposing (Decoder, ObjCoordinates)
import Physics.Coordinates exposing (BodyCoordinates)
import Point3d exposing (Point3d)
import Quantity exposing (Unitless)
import TriangularMesh exposing (TriangularMesh)
import Vector3d exposing (Vector3d)


bodyFrame : Frame3d Meters BodyCoordinates { defines : ObjCoordinates }
bodyFrame =
    Frame3d.atOrigin


type alias TexturedMesh =
    TriangularMesh
        { position : Point3d Meters BodyCoordinates
        , normal : Vector3d Unitless BodyCoordinates
        , uv : ( Float, Float )
        }


type alias ConvexMesh =
    TriangularMesh (Point3d Meters BodyCoordinates)


{-| Decodes a single physics mesh and a single rendering mesh
-}
physics1render1 : String -> String -> Decoder ( ConvexMesh, TexturedMesh )
physics1render1 convexMeshName meshName =
    Obj.Decode.map2 Tuple.pair
        (Obj.Decode.object convexMeshName (Obj.Decode.trianglesIn bodyFrame))
        (Obj.Decode.object meshName (Obj.Decode.texturedFacesIn bodyFrame))
