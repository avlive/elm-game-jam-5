module Screens.Stacking exposing (Model, Msg, init, onKeyChange, subscriptions, update, view)

{-| This demo loads a convex shape and a mesh from the same OBJ file.

  - elm-physics is used for simulation
  - elm-3d-scene is used for rendering

It is important to keep the convex shape as small as possible, because
this affects the simulation performance.

-}

import Acceleration
import Angle
import AngularSpeed
import Array exposing (Array)
import Assets exposing (Assets)
import Axis3d
import Beepbox
import Block3d exposing (Block3d)
import Browser.Events
import Camera3d
import Color exposing (Color)
import Direction3d
import Duration exposing (Duration)
import Element exposing (Element, alignBottom, alignLeft, alignRight, alignTop, centerX, centerY, column, el, height, html, inFront, none, padding, paragraph, px, rgb, rgb255, row, spacing, text, width)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Frame3d exposing (Frame3d)
import HighScores exposing (Score)
import Keyboard
import Keyboard.Arrows
import Length exposing (Length, Meters)
import Levels exposing (Level)
import LineSegment3d
import Mass
import Meshes exposing (ConvexMesh, TexturedMesh)
import Music
import Physics.Body exposing (Body)
import Physics.Body.Extra
import Physics.Coordinates exposing (BodyCoordinates, WorldCoordinates)
import Physics.Shape
import Physics.World exposing (World)
import Pixels exposing (Pixels)
import Point3d exposing (Point3d)
import Quantity exposing (Quantity, Unitless)
import Scene3d
import Scene3d.Material as Material exposing (Texture)
import Scene3d.Mesh exposing (Shadow, Textured)
import Screen exposing (Screen)
import Speed
import TriangularMesh exposing (TriangularMesh)
import Vector3d exposing (Vector3d)
import Viewpoint3d


cursorBounds : Length
cursorBounds =
    Length.meters 5.0


newPieceStart : Point3d Meters WorldCoordinates
newPieceStart =
    Point3d.meters 0 0 10


floorZ : Length
floorZ =
    Length.meters -3


type Data
    = LevelObject
    | Floor


floorBlock : Block3d Meters BodyCoordinates
floorBlock =
    Block3d.centeredOn Frame3d.atOrigin
        ( Length.meters 25, Length.meters 25, Length.millimeters 10 )


type alias Model =
    { level : Level
    , material : Material.Textured BodyCoordinates
    , object : Body Data
    , objectMesh : Textured BodyCoordinates
    , objectShadow : Shadow BodyCoordinates
    , objectVertices : Array (Point3d Meters BodyCoordinates)
    , timeLag : Duration
    , state : ActiveGameState
    }


type alias PhysicsModel =
    { world : World Data
    , maxPoint : Point3d Meters WorldCoordinates
    }


type ActiveGameState
    = MorePieces MorePiecesModel
    | LastPiece LastPieceModel
    | ShowScore ShowScoreModel


getPhysics : ActiveGameState -> Maybe PhysicsModel
getPhysics state =
    case state of
        MorePieces morePiecesModel ->
            Just morePiecesModel.physics

        LastPiece lastPieceModel ->
            Just lastPieceModel.physics

        ShowScore showScoreModel ->
            Just showScoreModel.physics


type alias MorePiecesModel =
    { physics : PhysicsModel
    , newPiecePosition : Frame3d Meters WorldCoordinates { defines : BodyCoordinates }
    , cursorPosition : Frame3d Meters WorldCoordinates { defines : BodyCoordinates }
    , timeUntilNewPieceSpawns : Duration
    , piecesRemaining : Int
    }


type alias LastPieceModel =
    { physics : PhysicsModel
    , settleTime : Duration
    }


initLastPiece : PhysicsModel -> LastPieceModel
initLastPiece physics =
    { physics = physics
    , settleTime = Duration.seconds 5
    }


type alias ShowScoreModel =
    { physics : PhysicsModel
    , peak : Point3d Meters WorldCoordinates
    , rotationTime : Duration
    }


initShowScore : PhysicsModel -> ShowScoreModel
initShowScore physics =
    { physics = physics
    , peak = physics.maxPoint
    , rotationTime = Quantity.zero
    }


type Msg
    = Tick Duration
    | MoveCursor { x : Int, y : Int }
    | ExitLevel


init : Level -> Assets.TexturedPhysicsObject -> ( Model, Cmd Msg )
init level object =
    let
        mesh =
            Scene3d.Mesh.texturedFaces object.mesh
                |> Scene3d.Mesh.cullBackFaces

        body =
            Physics.Body.compound
                [ Physics.Shape.unsafeConvex object.convex ]
                LevelObject
                |> Physics.Body.withBehavior (Physics.Body.dynamic (Mass.kilograms 1))
    in
    ( { level = level
      , material = object.material
      , object = body
      , objectMesh = mesh
      , objectShadow = Scene3d.Mesh.shadow mesh
      , objectVertices =
            TriangularMesh.vertices object.mesh
                |> Array.map .position
      , timeLag = Quantity.zero
      , state =
            MorePieces
                { physics = initPhysics body
                , newPiecePosition = Frame3d.atPoint newPieceStart
                , cursorPosition = Frame3d.atOrigin
                , timeUntilNewPieceSpawns = Duration.seconds 5
                , piecesRemaining = level.normalPieces
                }
      }
    , Beepbox.play 0.5 level.music
    )


initPhysics : Body Data -> PhysicsModel
initPhysics object =
    { world =
        Physics.World.empty
            |> Physics.World.withGravity
                (Acceleration.metersPerSecondSquared 9.80665)
                Direction3d.negativeZ
            |> Physics.World.add
                (Physics.Body.plane Floor
                    |> Physics.Body.moveTo (Point3d.meters 0 0 (Length.inMeters floorZ))
                )
            |> Physics.World.add
                (object
                    |> Physics.Body.rotateAround Axis3d.x (Angle.degrees 12)
                    |> Physics.Body.moveTo (Point3d.meters 0 0 0)
                )
    , maxPoint = Point3d.xyz Quantity.zero Quantity.zero floorZ
    }


update : Msg -> Model -> Result ( Level, Score ) ( Model, Cmd Msg )
update msg model =
    case ( msg, model.state ) of
        ( Tick tickDelta, _ ) ->
            Ok
                ( { model
                    | timeLag = Quantity.plus model.timeLag tickDelta
                  }
                    |> stepGame 3
                , Cmd.none
                )

        ( MoveCursor arrows, MorePieces morePiecesModel ) ->
            let
                newPosition =
                    morePiecesModel.cursorPosition
                        |> Frame3d.translateBy (Vector3d.meters -(toFloat arrows.x) -(toFloat arrows.y) 0)

                clampedPosition =
                    if
                        Frame3d.originPoint newPosition
                            |> Point3d.distanceFrom Point3d.origin
                            |> Quantity.lessThanOrEqualTo cursorBounds
                    then
                        newPosition

                    else
                        morePiecesModel.cursorPosition
            in
            Ok
                ( { model
                    | state =
                        MorePieces
                            { morePiecesModel
                                | cursorPosition = clampedPosition
                            }
                  }
                , Cmd.none
                )

        ( MoveCursor _, _ ) ->
            Ok ( model, Cmd.none )

        ( ExitLevel, ShowScore showScoreModel ) ->
            Err
                ( model.level
                , toScore showScoreModel.peak
                )

        ( ExitLevel, _ ) ->
            Ok ( model, Cmd.none )


toScore : Point3d Meters WorldCoordinates -> Score
toScore point =
    Point3d.zCoordinate point
        |> Quantity.minus floorZ
        |> Length.inMillimeters
        |> round


stepGame : Int -> Model -> Model
stepGame maxSteps model =
    let
        frameTime =
            Duration.milliseconds 40
    in
    if maxSteps <= 0 then
        model

    else if Quantity.lessThanOrEqualTo frameTime model.timeLag then
        model

    else
        -- NOTE: the recursive call is made as a function call because using `|>` would prevent tail call elimination in Elm 0.19
        stepGame
            (maxSteps - 1)
            ({ model | timeLag = Quantity.minus frameTime model.timeLag }
                |> updateGameState frameTime
            )


simulateWorld : Array (Point3d Meters BodyCoordinates) -> Duration -> PhysicsModel -> PhysicsModel
simulateWorld objectVertices delta model =
    let
        newWorld =
            Physics.World.simulate delta model.world
    in
    { model
        | world = newWorld
        , maxPoint = computeMax objectVertices newWorld
    }


updateGameState : Duration -> Model -> Model
updateGameState delta model =
    case model.state of
        MorePieces morePiecesModel ->
            { model
                | state =
                    { morePiecesModel
                        | physics = simulateWorld model.objectVertices delta morePiecesModel.physics
                    }
                        |> advanceNewPiece delta
                        |> spawnNewPiece model.object
            }

        LastPiece lastPieceModel ->
            { model
                | state =
                    { lastPieceModel
                        | settleTime = Quantity.minus delta lastPieceModel.settleTime
                        , physics = simulateWorld model.objectVertices delta lastPieceModel.physics
                    }
                        |> checkLastPieceWait
            }

        ShowScore showScoreModel ->
            { model
                | state =
                    { showScoreModel
                        | rotationTime = Quantity.plus delta showScoreModel.rotationTime
                    }
                        |> ShowScore
            }


checkLastPieceWait : LastPieceModel -> ActiveGameState
checkLastPieceWait model =
    if Quantity.lessThanOrEqualTo Quantity.zero model.settleTime then
        ShowScore (initShowScore model.physics)

    else
        LastPiece model


advanceNewPiece : Duration -> MorePiecesModel -> MorePiecesModel
advanceNewPiece delta model =
    { model
        | newPiecePosition =
            interpolateTowards
                (Quantity.ratio delta model.timeUntilNewPieceSpawns)
                model.cursorPosition
                model.newPiecePosition
        , timeUntilNewPieceSpawns =
            model.timeUntilNewPieceSpawns
                |> Quantity.minus delta
    }


interpolateTowards : Float -> Frame3d a b c -> Frame3d a b c -> Frame3d a b c
interpolateTowards percent target start =
    -- TODO: handle rotation
    let
        translate =
            Vector3d.from (Frame3d.originPoint start) (Frame3d.originPoint target)
                |> Vector3d.scaleBy percent
    in
    start
        |> Frame3d.translateBy translate


computeMax : Array (Point3d Meters BodyCoordinates) -> World Data -> Point3d Meters WorldCoordinates
computeMax vertices world =
    let
        step body max =
            case Physics.Body.data body of
                Floor ->
                    max

                LevelObject ->
                    let
                        frame : Frame3d Meters WorldCoordinates { defines : BodyCoordinates }
                        frame =
                            Physics.Body.frame body
                    in
                    arrayMaxBy Point3d.zCoordinate
                        max
                        (Array.map (Point3d.placeIn frame) vertices)
    in
    List.foldl step (Point3d.xyz Quantity.zero Quantity.zero floorZ) (Physics.World.bodies world)


arrayMaxBy : (a -> Quantity Float units) -> a -> Array a -> a
arrayMaxBy get first array =
    Array.foldl
        (\a b ->
            if Quantity.greaterThan (get a) (get b) then
                b

            else
                a
        )
        first
        array


{-| Checks if it's time to spawn a new piece and does so if it is.
-}
spawnNewPiece : Body Data -> MorePiecesModel -> ActiveGameState
spawnNewPiece object model =
    if
        model.timeUntilNewPieceSpawns
            |> Quantity.lessThanOrEqualTo Quantity.zero
    then
        let
            newPhysics =
                model.physics
                    |> physicsAdd object model.cursorPosition
        in
        case model.piecesRemaining - 1 of
            0 ->
                LastPiece (initLastPiece newPhysics)

            _ ->
                MorePieces
                    { model
                        | timeUntilNewPieceSpawns = Duration.seconds 5
                        , piecesRemaining = model.piecesRemaining - 1
                        , newPiecePosition = Frame3d.atPoint newPieceStart
                        , physics = newPhysics
                    }

    else
        MorePieces model


physicsAdd : Body Data -> Frame3d Meters WorldCoordinates { defines : BodyCoordinates } -> PhysicsModel -> PhysicsModel
physicsAdd object frame physics =
    { physics
        | world =
            physics.world
                |> Physics.World.add (Physics.Body.Extra.moveToFrame frame object)
    }


onKeyChange : Keyboard.KeyChange -> Model -> Maybe Msg
onKeyChange change model =
    case model.state of
        MorePieces _ ->
            onKeyChangeMorePieces change

        LastPiece _ ->
            Nothing

        ShowScore _ ->
            onKeyChangeShowScore change


onKeyChangeMorePieces : Keyboard.KeyChange -> Maybe Msg
onKeyChangeMorePieces change =
    case change of
        Keyboard.KeyDown key ->
            Just (MoveCursor (Keyboard.Arrows.arrows [ key ]))

        _ ->
            Nothing


onKeyChangeShowScore : Keyboard.KeyChange -> Maybe Msg
onKeyChangeShowScore change =
    case change of
        Keyboard.KeyDown Keyboard.Enter ->
            Just ExitLevel

        _ ->
            Nothing


updateDropPosition : Level -> Duration -> Frame3d Meters coord defines -> Frame3d Meters coord defines
updateDropPosition level d =
    let
        center =
            Levels.objectMeshCenter level
    in
    Frame3d.translateBy (Vector3d.from center Point3d.origin)
        >> Frame3d.rotateAround Axis3d.x (Angle.degrees (360 * Duration.inSeconds d / 2))
        >> Frame3d.translateBy (Vector3d.from Point3d.origin center)


view : ( Quantity Int Pixels, Quantity Int Pixels ) -> Score -> Model -> Screen Msg
view dimensions highScore model =
    { html =
        \() ->
            viewActive dimensions model.level highScore model
    , music = Just Music.testSong
    }


viewActive : ( Quantity Int Pixels, Quantity Int Pixels ) -> Level -> Score -> Model -> Element Msg
viewActive ( w, h ) level highScore activeModel =
    let
        mainWidth =
            w
                |> Quantity.toFloatQuantity
                |> Quantity.multiplyBy 0.7
                |> Quantity.floor

        sideWidth =
            w |> Quantity.minus mainWidth
    in
    row
        []
        [ el
            [ height (px (Pixels.toInt h))
            , inFront <|
                el
                    [ alignRight
                    , alignTop
                    ]
                    (hudView activeModel)
            , inFront <|
                el
                    [ centerX
                    , centerY
                    ]
                    (popupView level highScore activeModel)
            , inFront <|
                el
                    [ alignBottom
                    , alignLeft
                    ]
                    (onScreenControls activeModel.state)
            ]
          <|
            liveScene ( mainWidth, h ) level activeModel
        , column
            [ width (px (Pixels.toInt sideWidth))
            , Background.color (rgb 0.7 0.6 0.5)
            ]
            [ sideScene ( sideWidth, Quantity.minus (Pixels.int 100) h ) level activeModel
            , case getPhysics activeModel.state of
                Just physics ->
                    text (HighScores.lengthString (toScore physics.maxPoint))

                Nothing ->
                    text ""
            ]
        ]


hudView : Model -> Element never
hudView model =
    case model.state of
        MorePieces morePiecesModel ->
            el
                [ padding 10
                , Font.size 40
                ]
            <|
                text (String.fromInt morePiecesModel.piecesRemaining)

        LastPiece lastPieceModel ->
            lastPieceModel.physics.world
                |> Physics.World.bodies
                |> List.map
                    (Physics.Body.velocity
                        >> Vector3d.length
                        >> Speed.inMetersPerSecond
                    )
                |> List.maximum
                |> Maybe.withDefault 0
                |> String.fromFloat
                |> text

        ShowScore _ ->
            none


popupView : Level -> Score -> Model -> Element Msg
popupView level highScore model =
    case model.state of
        ShowScore showScoreModel ->
            Element.column
                [ Background.color (Element.rgba255 108 96 96 0.7)
                , Border.rounded 15
                , Font.color (Element.rgba255 255 255 255 1)
                , Element.spacingXY 0 40
                , Element.width Element.fill
                , Element.paddingXY 40 40
                ]
                [ Element.paragraph
                    [ Font.center
                    , Font.bold
                    , Font.color (Element.rgba255 191 237 253 1)
                    , Font.size 36
                    , Element.width Element.fill
                    , Region.heading 1
                    ]
                    [ Element.text "Weather Report" ]
                , let
                    newScore =
                        toScore showScoreModel.peak
                  in
                  if newScore > highScore then
                    column
                        [ spacing 10
                        ]
                        [ Element.paragraph
                            [ Font.center
                            ]
                            [ text "😮  Today's "
                            , text level.objectNamefall
                            , text " has reached a "
                            , el [ Font.extraBold ] (text "historic high")
                            , text " of "
                            , el [ Font.extraBold ] (text (HighScores.lengthString newScore))
                            , text "!  😮"
                            ]
                        , paragraph
                            [ Font.center
                            , Font.size 14
                            ]
                            [ text "previous high: "
                            , text (HighScores.lengthString highScore)
                            ]
                        ]

                  else
                    column
                        [ spacing 10
                        ]
                        [ paragraph
                            [ Font.center
                            ]
                            [ text "Today's "
                            , text level.objectNamefall
                            , text ": "
                            , el [ Font.extraBold ] (text (HighScores.lengthString newScore))
                            ]
                        , paragraph
                            [ Font.center
                            , Font.size 14
                            ]
                            [ text "historic high: "
                            , text (HighScores.lengthString highScore)
                            ]
                        ]
                , Input.button
                    [ Background.color (Element.rgba255 52 101 164 1)
                    , Element.centerX
                    , Font.center
                    , Font.color (Element.rgba255 255 255 255 1)
                    , Element.paddingXY 16 8
                    , Border.color (Element.rgba255 52 101 164 1)
                    , Border.solid
                    , Border.widthXY 1 1
                    ]
                    { onPress = Just ExitLevel
                    , label = Element.text "What's falling next?!"
                    }
                ]

        _ ->
            none


onScreenControls : ActiveGameState -> Element Msg
onScreenControls state =
    case state of
        MorePieces _ ->
            let
                b label x y =
                    Input.button
                        [ width (px 45)
                        , height (px 45)
                        , Background.color (rgb255 80 180 220)
                        , Font.color (rgb255 30 30 50)
                        , Border.rounded 10
                        , centerX
                        , Font.center
                        ]
                        { label = text label
                        , onPress = Just (MoveCursor { x = x, y = y })
                        }
            in
            column
                [ spacing 8
                , padding 10
                ]
                [ b "⬆" 0 1
                , row [ spacing 8 ]
                    [ b "⬅" -1 0
                    , b "➡" 1 0
                    ]
                , b "⬇" 0 -1
                ]

        _ ->
            none


liveScene : ( Quantity Int Pixels, Quantity Int Pixels ) -> Level -> Model -> Element never
liveScene dimensions level activeModel =
    let
        camera =
            case activeModel.state of
                ShowScore showScoreModel ->
                    Camera3d.perspective
                        { viewpoint =
                            Viewpoint3d.orbitZ
                                { focalPoint = showScoreModel.peak
                                , azimuth =
                                    AngularSpeed.degreesPerSecond 60
                                        |> Quantity.for showScoreModel.rotationTime
                                , elevation = Angle.degrees 5
                                , distance = Length.meters 15
                                }
                        , verticalFieldOfView = Angle.degrees 30
                        }

                _ ->
                    Camera3d.perspective
                        { viewpoint =
                            Viewpoint3d.orbitZ
                                { focalPoint = Point3d.meters 0 0 0
                                , azimuth = Angle.degrees 45
                                , elevation = Angle.degrees 25
                                , distance = Length.meters 25
                                }
                        , verticalFieldOfView = Angle.degrees 30
                        }

        newPieceMaterial =
            Levels.cursorMaterial level

        cursorMaterial =
            Material.matte Color.white
    in
    Scene3d.sunny
        { upDirection = Direction3d.z
        , sunlightDirection = Direction3d.negativeZ
        , shadows = True
        , camera = camera
        , dimensions = dimensions
        , background = Scene3d.transparentBackground
        , clipDepth = Length.meters 0.1
        , entities =
            [ case activeModel.state of
                MorePieces morePiecesModel ->
                    [ Scene3d.meshWithShadow newPieceMaterial activeModel.objectMesh activeModel.objectShadow
                        |> Scene3d.placeIn morePiecesModel.newPiecePosition
                    , Scene3d.meshWithShadow cursorMaterial activeModel.objectMesh activeModel.objectShadow
                        |> Scene3d.placeIn morePiecesModel.cursorPosition

                    -- attempt at showing the cursor bounds:
                    --, Scene3d.cylinder (Material.color Color.purple)
                    --    (Cylinder3d.centeredOn
                    --        Point3d.origin
                    --        Direction3d.z
                    --        { radius = cursorBounds
                    --        , length = Length.meters 0.2
                    --        }
                    --    )
                    ]

                LastPiece _ ->
                    []

                ShowScore _ ->
                    []
            , case getPhysics activeModel.state of
                Just physics ->
                    worldBodies activeModel.objectMesh activeModel.objectShadow physics.world activeModel.material

                Nothing ->
                    []
            ]
                |> List.concat
        }
        |> html


sideScene : ( Quantity Int Pixels, Quantity Int Pixels ) -> Level -> Model -> Element never
sideScene dimensions level activeModel =
    let
        camera =
            Camera3d.orthographic
                { viewpoint =
                    Viewpoint3d.orbitZ
                        { focalPoint = Point3d.meters 0 0 0
                        , azimuth = Angle.degrees 90
                        , elevation = Angle.degrees 0
                        , distance = Length.meters 25
                        }
                , viewportHeight = Length.meters 10
                }
    in
    Scene3d.unlit
        { camera = camera
        , dimensions = dimensions
        , background = Scene3d.transparentBackground
        , clipDepth = Length.meters 0.1
        , entities =
            case getPhysics activeModel.state of
                Just physics ->
                    let
                        maxZ =
                            Point3d.zCoordinate physics.maxPoint
                    in
                    [ Scene3d.lineSegment
                        (Material.color Color.red)
                        (LineSegment3d.from
                            (Point3d.xyz (Length.meters -5) (Length.meters -5) maxZ)
                            (Point3d.xyz (Length.meters 5) (Length.meters 5) maxZ)
                        )
                    ]
                        ++ worldBodies activeModel.objectMesh activeModel.objectShadow physics.world (Material.color Color.darkBlue)

                Nothing ->
                    []
        }
        |> html


worldBodies : Textured BodyCoordinates -> Shadow BodyCoordinates -> World Data -> Material.Textured BodyCoordinates -> List (Scene3d.Entity Physics.Coordinates.WorldCoordinates)
worldBodies mesh shadow world material =
    world
        |> Physics.World.bodies
        |> List.map (bodyToEntity mesh shadow material)


bodyToEntity : Textured BodyCoordinates -> Shadow BodyCoordinates -> Material.Textured BodyCoordinates -> Body Data -> Scene3d.Entity Physics.Coordinates.WorldCoordinates
bodyToEntity mesh shadow material body =
    let
        frame3d =
            Physics.Body.frame body
    in
    dataToEntity mesh shadow material (Physics.Body.data body)
        |> Scene3d.placeIn frame3d


dataToEntity : Textured BodyCoordinates -> Shadow BodyCoordinates -> Material.Textured BodyCoordinates -> Data -> Scene3d.Entity BodyCoordinates
dataToEntity mesh shadow material data =
    case data of
        LevelObject ->
            Scene3d.meshWithShadow material mesh shadow

        Floor ->
            Scene3d.block (Material.matte Color.darkCharcoal) floorBlock


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onAnimationFrameDelta (Duration.milliseconds >> Tick)
