module Screens.Menu exposing (Model, Msg, init, onKeyChange, subscriptions, update, view)

import Angle
import Assets
import Browser.Events
import Camera3d
import Dict
import Direction3d
import Duration exposing (Duration)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Frame3d exposing (Frame3d)
import HighScores exposing (HighScores)
import Keyboard
import Length exposing (Meters)
import Levels exposing (Level)
import Physics.Coordinates exposing (BodyCoordinates, WorldCoordinates)
import Pixels exposing (Pixels)
import Point3d
import Quantity exposing (Quantity)
import Random exposing (Generator)
import Scene3d
import Scene3d.Material as Material
import Scene3d.Mesh exposing (Shadow, Textured)
import Screen exposing (Screen)
import Speed
import Viewpoint3d


type alias Model =
    { material : Material.Textured BodyCoordinates
    , objectMesh : Textured BodyCoordinates
    , objectShadow : Shadow BodyCoordinates
    , fallingObjects : List FallingObject
    }


type alias FallingObject =
    { frame : Frame3d Meters WorldCoordinates { defines : BodyCoordinates }
    }


newFallingObject : Generator FallingObject
newFallingObject =
    let
        make r a =
            { frame =
                Frame3d.atPoint (Point3d.meters 0 0 500)
                    |> Frame3d.translateIn (Direction3d.xy (Angle.degrees a)) (Length.meters r)
            }
    in
    Random.map2 make (Random.float 0.03 1.2) (Random.float 0 360)


init : Assets.TexturedPhysicsObject -> ( Model, Cmd Msg )
init object =
    let
        mesh =
            Scene3d.Mesh.texturedFaces object.mesh
                |> Scene3d.Mesh.cullBackFaces
    in
    { material = object.material
    , objectMesh = mesh
    , objectShadow = Scene3d.Mesh.shadow mesh
    , fallingObjects = []
    }
        |> spawnNew


type Msg
    = ChooseCurrent
    | Tick Duration
    | NewFallingObject FallingObject


subscriptions : Model -> Sub Msg
subscriptions _ =
    Browser.Events.onAnimationFrameDelta (Duration.milliseconds >> Tick)


update : Msg -> Model -> Result Level ( Model, Cmd Msg )
update msg model =
    case msg of
        ChooseCurrent ->
            Err Levels.duckling

        Tick delta ->
            { model
                | fallingObjects = List.filterMap (updateFallingObject delta) model.fallingObjects
            }
                |> spawnNew
                |> Ok

        NewFallingObject fallingObject ->
            { model | fallingObjects = fallingObject :: model.fallingObjects }
                |> spawnNew
                |> Ok


updateFallingObject : Quantity Float Duration.Seconds -> FallingObject -> Maybe FallingObject
updateFallingObject delta object =
    let
        velocity =
            Speed.metersPerSecond -150

        newFrame =
            object.frame
                |> Frame3d.translateIn Direction3d.z (Quantity.for delta velocity)
    in
    if
        Point3d.zCoordinate (Frame3d.originPoint newFrame)
            |> Quantity.lessThan Quantity.zero
    then
        Nothing

    else
        Just
            { object
                | frame = newFrame
            }


spawnNew : Model -> ( Model, Cmd Msg )
spawnNew model =
    ( model
    , if List.isEmpty model.fallingObjects then
        Random.generate NewFallingObject newFallingObject

      else
        Cmd.none
    )


onKeyChange : Keyboard.KeyChange -> Model -> Maybe Msg
onKeyChange keyChange model =
    case keyChange of
        Keyboard.KeyDown Keyboard.Enter ->
            Just ChooseCurrent

        _ ->
            Nothing


view : ( Quantity Int Pixels, Quantity Int Pixels ) -> HighScores -> Model -> Screen Msg
view dimensions scores model =
    let
        camera =
            Camera3d.perspective
                { viewpoint =
                    Viewpoint3d.orbitZ
                        { focalPoint = Point3d.meters 0 0 50
                        , azimuth = Angle.degrees 45
                        , elevation = Angle.degrees -89.4
                        , distance = Length.meters 50
                        }
                , verticalFieldOfView = Angle.degrees 30
                }
    in
    { html =
        \() ->
            Scene3d.sunny
                { upDirection = Direction3d.z
                , sunlightDirection = Direction3d.negativeZ
                , shadows = True
                , camera = camera
                , dimensions = dimensions
                , background = Scene3d.transparentBackground
                , clipDepth = Length.meters 0.1
                , entities =
                    model.fallingObjects
                        |> List.map
                            (\object ->
                                Scene3d.meshWithShadow
                                    model.material
                                    model.objectMesh
                                    model.objectShadow
                                    |> Scene3d.placeIn object.frame
                            )
                }
                |> html
                |> el
                    [ inFront <|
                        column
                            [ Font.size 30
                            , centerX
                            , centerY
                            , spacing 30
                            ]
                            [ paragraph
                                [ padding 20
                                , width (fill |> maximum 800)
                                ]
                                [ text "Instructions: "
                                , text "Use the arrow keys (or on-screen buttons) to aim the falling pieces.  Try to build the tallest stack."
                                ]
                            , Input.button
                                [ Background.color (rgb255 80 180 220)
                                , Font.color (rgb255 30 30 50)
                                , Border.rounded 10
                                , centerX
                                , Font.center
                                , padding 15
                                ]
                                { label = text "Let them fall!"
                                , onPress = Just ChooseCurrent
                                }
                            , case Dict.get Levels.duckling.id scores of
                                Nothing ->
                                    none

                                Just score ->
                                    paragraph
                                        [ centerX
                                        , Font.center
                                        ]
                                        [ text "High score: "
                                        , text (HighScores.lengthString score)
                                        ]
                            ]
                    ]
    , music = Nothing
    }
