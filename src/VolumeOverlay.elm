module VolumeOverlay exposing (view)

import Element exposing (..)
import Element.Font as Font
import Element.Input as Input


view : (Float -> msg) -> Float -> Element msg
view onChange volume =
    el
        [ alignTop
        , alignRight
        ]
    <|
        if volume > 0 then
            Input.button
                [ Font.size 40
                ]
                { onPress = Just (onChange 0)
                , label = text "🔊"
                }

        else
            Input.button
                [ Font.size 40
                ]
                { onPress = Just (onChange 0.5)
                , label = text "🔇"
                }
