port module HighScores exposing (HighScores, Score, lengthString, saveScore)

import Dict exposing (Dict)
import FormatNumber
import FormatNumber.Locales exposing (Decimals(..))
import Length exposing (Length)
import Levels exposing (LevelId)


type alias HighScores =
    Dict LevelId Score


type alias Score =
    -- in millimeters
    Int


lengthString : Score -> String
lengthString l =
    if l <= 0 then
        "zero"

    else
        FormatNumber.format
            { decimals = Exact 3
            , thousandSeparator = ","
            , decimalSeparator = "."
            , negativePrefix = "-"
            , negativeSuffix = ""
            , positivePrefix = ""
            , positiveSuffix = ""
            , zeroPrefix = ""
            , zeroSuffix = ""
            }
            (Length.inMeters (Length.millimeters (toFloat l)))
            ++ " meters"


port saveScore : ( LevelId, Score ) -> Cmd msg
