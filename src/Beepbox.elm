port module Beepbox exposing (Song, play, setVolume)


type alias Song =
    String


play : Float -> Song -> Cmd msg
play volume song =
    beepboxPlay
        { song = song
        , volume = volume
        }


setVolume : Float -> Cmd msg
setVolume =
    beepboxSetVolume


port beepboxPlay : { song : String, volume : Float } -> Cmd msg


port beepboxSetVolume : Float -> Cmd msg
