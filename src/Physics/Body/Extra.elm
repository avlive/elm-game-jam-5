module Physics.Body.Extra exposing (moveToFrame)

import Axis3d
import Direction3d
import Frame3d exposing (Frame3d)
import Length exposing (Meters)
import Physics.Body exposing (Body)
import Physics.Coordinates exposing (WorldCoordinates)


{-| XXX: this doesn't yet handle frame rotation around the y/z axes, and is also incorrect for x-axis rotations greater than 180 degrees.
-}
moveToFrame : Frame3d Meters WorldCoordinates c -> Body data -> Body data
moveToFrame frame =
    Physics.Body.rotateAround Axis3d.x (Direction3d.angleFrom Direction3d.z (Frame3d.zDirection frame))
        >> Physics.Body.moveTo (Frame3d.originPoint frame)
