module Main exposing (main)

import Assets exposing (Assets)
import Beepbox
import Browser
import Browser.Dom
import Browser.Events
import Dict exposing (Dict)
import Element exposing (..)
import HighScores exposing (HighScores)
import Keyboard
import Levels exposing (Level, LevelId)
import Pixels exposing (Pixels)
import Quantity exposing (Quantity)
import Screen
import Screens.Menu as Menu
import Screens.Stacking as Stacking
import Task
import VolumeOverlay


type alias Flags =
    { scores : List ( String, Int )
    }


type alias Model =
    { dimensions : ( Quantity Int Pixels, Quantity Int Pixels )
    , pressedKeys : List Keyboard.Key
    , volume : Float
    , playingMusic : Maybe Beepbox.Song
    , screen : Screen
    , scoresMm : HighScores
    , assets : Assets
    }


type Screen
    = Menu Menu.Model
    | Stacking Stacking.Model
    | Loading ScreenId


type ScreenId
    = Menu_
    | Stacking_ Level


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( { dimensions = ( Pixels.int 0, Pixels.int 0 )
      , pressedKeys = []
      , volume = 0.5
      , playingMusic = Nothing
      , screen = Loading Menu_
      , scoresMm = Dict.fromList flags.scores
      , assets = Assets.init
      }
    , Cmd.batch
        [ Task.perform
            (\{ viewport } -> Resize (round viewport.width) (round viewport.height))
            Browser.Dom.getViewport
        , Assets.load |> Cmd.map AssetsMsg
        ]
    )
        |> loadWhenReady Menu_
        |> updateMusic


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onResize Resize
        , Sub.map KeyboardMsg Keyboard.subscriptions
        , case model.screen of
            Menu screenModel ->
                Sub.map MenuMsg (Menu.subscriptions screenModel)

            Stacking screenModel ->
                Sub.map StackingMsg (Stacking.subscriptions screenModel)

            Loading _ ->
                Sub.none
        ]


type Msg
    = Resize Int Int
    | KeyboardMsg Keyboard.Msg
    | SetVolume Float
    | StackingMsg Stacking.Msg
    | MenuMsg Menu.Msg
    | AssetsMsg Assets.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case ( msg, model.screen ) of
        ( Resize width height, _ ) ->
            ( { model | dimensions = ( Pixels.int width, Pixels.int height ) }
            , Cmd.none
            )

        ( SetVolume volume, _ ) ->
            ( { model | volume = volume }
            , Beepbox.setVolume volume
            )

        ( KeyboardMsg keyboardMsg, _ ) ->
            let
                ( newPressedKeys, maybeChange ) =
                    Keyboard.updateWithKeyChange Keyboard.anyKeyUpper keyboardMsg model.pressedKeys

                modelWithKeys =
                    { model
                        | pressedKeys = newPressedKeys
                    }
            in
            Maybe.map (\keyChange -> onKeyChange keyChange modelWithKeys) maybeChange
                |> Maybe.withDefault ( modelWithKeys, Cmd.none )

        ( StackingMsg screenMsg, Stacking screenModel ) ->
            case Stacking.update screenMsg screenModel of
                Ok ( newScreenModel, screenCmd ) ->
                    ( { model | screen = Stacking newScreenModel }
                    , Cmd.map StackingMsg screenCmd
                    )
                        |> updateMusic

                Err ( level, score ) ->
                    (if score > (Dict.get level.id model.scoresMm |> Maybe.withDefault 0) then
                        ( { model
                            | scoresMm =
                                model.scoresMm
                                    |> Dict.insert level.id score
                          }
                        , HighScores.saveScore ( level.id, score )
                        )

                     else
                        ( model
                        , Cmd.none
                        )
                    )
                        |> loadWhenReady Menu_

        ( StackingMsg _, _ ) ->
            ( model, Cmd.none )

        ( MenuMsg screenMsg, Menu screenModel ) ->
            case Menu.update screenMsg screenModel of
                Ok ( newScreenModel, cmd ) ->
                    ( { model | screen = Menu newScreenModel }
                    , Cmd.map MenuMsg cmd
                    )
                        |> updateMusic

                Err level ->
                    ( model, Cmd.none )
                        |> loadWhenReady (Stacking_ level)

        ( MenuMsg _, _ ) ->
            ( model, Cmd.none )

        ( AssetsMsg assetsMsg, _ ) ->
            let
                newModel =
                    { model | assets = Assets.update assetsMsg model.assets }
            in
            case model.screen of
                Loading screenId ->
                    ( newModel, Cmd.none )
                        |> loadWhenReady screenId

                _ ->
                    ( newModel, Cmd.none )


loadWhenReady : ScreenId -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
loadWhenReady screenId ( model, cmd ) =
    case Assets.duck model.assets of
        Nothing ->
            ( { model | screen = Loading screenId }
            , cmd
            )

        Just object ->
            case screenId of
                Stacking_ level ->
                    Stacking.init level object
                        |> Tuple.mapBoth (\m -> { model | screen = Stacking m }) (Cmd.map StackingMsg)
                        |> Tuple.mapSecond (\c -> Cmd.batch [ cmd, c ])

                Menu_ ->
                    Menu.init object
                        |> Tuple.mapBoth (\m -> { model | screen = Menu m }) (Cmd.map MenuMsg)
                        |> Tuple.mapSecond (\c -> Cmd.batch [ cmd, c ])


onKeyChange : Keyboard.KeyChange -> Model -> ( Model, Cmd Msg )
onKeyChange keyChange model =
    case keyChange of
        Keyboard.KeyDown Keyboard.Escape ->
            -- For debugging
            ( model
            , Beepbox.setVolume 0
            )
                |> loadWhenReady Menu_

        _ ->
            let
                maybeMsg =
                    case model.screen of
                        Stacking screenModel ->
                            Stacking.onKeyChange keyChange screenModel
                                |> Maybe.map StackingMsg

                        Menu screenModel ->
                            Menu.onKeyChange keyChange screenModel
                                |> Maybe.map MenuMsg

                        Loading _ ->
                            Nothing
            in
            case maybeMsg of
                Just msg ->
                    update msg model

                Nothing ->
                    ( model, Cmd.none )


updateMusic : ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
updateMusic ( model, cmd ) =
    let
        requestedMusic =
            (currentScreen model).music
    in
    if model.playingMusic /= requestedMusic then
        ( { model | playingMusic = requestedMusic }
        , Cmd.batch
            [ cmd
            , Beepbox.play model.volume (requestedMusic |> Maybe.withDefault "")
            ]
        )

    else
        ( model, cmd )


view : Model -> Browser.Document Msg
view model =
    let
        screen =
            currentScreen model
    in
    { title = "avh4/elm-game-jam-5"
    , body =
        [ screen.html ()
            |> layout
                [ width fill
                , height fill
                , inFront (VolumeOverlay.view SetVolume model.volume)
                ]
        ]
    }


currentScreen : Model -> Screen.Screen Msg
currentScreen model =
    case model.screen of
        Menu screenModel ->
            Menu.view model.dimensions model.scoresMm screenModel
                |> Screen.map MenuMsg

        Stacking screenModel ->
            let
                highScore =
                    Dict.get screenModel.level.id model.scoresMm
                        |> Maybe.withDefault 0
            in
            Stacking.view model.dimensions highScore screenModel
                |> Screen.map StackingMsg

        Loading _ ->
            { html = \() -> text "Loading texture and meshes…"
            , music = Nothing
            }


main : Program Flags Model Msg
main =
    Browser.document
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }
