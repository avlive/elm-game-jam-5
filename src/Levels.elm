module Levels exposing (Level, LevelId, cursorMaterial, duckling, objectMeshCenter)

import Assets
import Beepbox
import Color exposing (Color)
import Length exposing (Meters)
import Meshes exposing (ConvexMesh, TexturedMesh)
import Music
import Obj.Decode exposing (Decoder)
import Point3d exposing (Point3d)
import Scene3d.Material as Material exposing (Material)


type alias Level =
    { id : LevelId
    , objectNamePlural : String
    , objectNamefall : String
    , object : Assets.MainObject
    , objectMeshCenter : { x : Float, y : Float, z : Float }
    , cursorColor : Color
    , music : Beepbox.Song
    , normalPieces : Int
    }


type alias LevelId =
    String


cursorMaterial : Level -> Material coordinates { a | normals : () }
cursorMaterial level =
    Material.matte level.cursorColor


objectMeshCenter : Level -> Point3d Meters coordinates
objectMeshCenter level =
    Point3d.meters level.objectMeshCenter.x level.objectMeshCenter.y level.objectMeshCenter.z


duckling : Level
duckling =
    { id = "a64f-4d6a"
    , objectNamePlural = "giant duckies"
    , objectNamefall = "giant duckiefall"
    , object = Assets.Duck
    , objectMeshCenter = { x = 0, y = 0, z = 0.3 }
    , cursorColor = Color.lightYellow
    , music = Music.testSong
    , normalPieces = 15
    }
