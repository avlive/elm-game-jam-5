module Screen exposing (Screen, map)

import Beepbox
import Element exposing (Element)


type alias Screen msg =
    { html : () -> Element msg
    , music : Maybe Beepbox.Song
    }


map : (a -> b) -> Screen a -> Screen b
map f screen =
    { html = \() -> Element.map f (screen.html ())
    , music = screen.music
    }
